<?php

namespace Tests;

use Mockery as m;
use App\Call;
use App\Contact;
use App\Mobile;
use App\Services\ContactService;
use PHPUnit\Framework\TestCase;

use App\Interfaces\CarrierInterface;

class MobileTest extends TestCase
{
	use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = m::mock(CarrierInterface::class);
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_return_a_call_to_valid_contact()
	{	
		$contact = new Contact();

        $provider = m::mock(CarrierInterface::class);
		
        $provider->shouldReceive('dialContact')
            ->once()->with($contact);
		
        $provider->shouldReceive('makeCall')
            ->once()->andReturn(new Call);

        $service = m::mock('alias:' . ContactService::class);
        $service->shouldReceive('findByName')->andReturn($contact);

		$mobile = new Mobile($provider);

        $this->assertInstanceOf(Call::class, $mobile->makeCallByName('John Doe'));
	}

	 /** @test */
	 public function it_should_return_false_if_the_contact_does_not_exit()
	 {
        $provider = m::mock(CarrierInterface::class);

        $service = m::mock('alias:' . ContactService::class);
        $service->shouldReceive('findByName')
            ->once()
            ->with('John Does')
            ->andReturnFalse();

        $mobile = new Mobile($provider);

        $this->assertFalse($mobile->makeCallByName('John Does'));
	}

}
